# README #

LED Matrix that responds to music beats. Check out the video in the repository.

### Setup ###

* Clone the repo
* Install Processing IDE in your system
* Run the `musicalLED.pde` code and enjoy ! 
 Make sure all the dependencies are installed/pre-configured.

### Arduino Connection ###

Take 3 RGB LEDs preferably and connect to Arduino Digital I/O pins as follows. Don't forget to put 10k resistors in between.

redPin1 = 12; 
greenPin1 = 11;  
bluePin1 = 13;  

redPin2 = 8;
greenPin2 = 7;
bluePin2 = 4;

redPin3 = 6;
greenPin3 = 5;
bluePin3 = 3;

### Arduino Setup ###
Make sure you can access Arduino pins via Processing IDE. Run the Arduino(Firmata) code on Arduino from the Arduino IDE. Follow the [documentation here](http://playground.arduino.cc/Interfacing/Processing).

### Contribution guidelines ###

* Writing tests
* Code review