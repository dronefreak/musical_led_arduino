import ddf.minim.*;            //Importing Minim Java library
import ddf.minim.analysis.*;  
import processing.serial.*;   //Serial Processing Library 
import cc.arduino.*;          //Importing Arduino library 

Arduino arduino;      //Here all the classes are created that will be used further, 
Minim minim;          // like the arduino, minim, song and the fft classes
AudioPlayer song;
FFT fft;

int redPin1 = 12;    //Pin Initialization
int greenPin1 = 11;  //for the three RGB LED's,
int bluePin1 = 13;  // each having individaually three led's red, green, blue

int redPin2 = 8;
int greenPin2 = 7;
int bluePin2 = 4;

int redPin3 = 6;
int greenPin3 = 5;
int bluePin3 = 3;

int color_id = 0;  

int common_cathode = 0;    //Config of the Led's

void setup() {
    size(800, 600);
    
    arduino = new Arduino(this, Arduino.list()[8], 57600);
    for (int i = 0; i <= 13; i++) arduino.pinMode(i, Arduino.OUTPUT);  //Setting pins as Output 
    for (int i = 0; i < 13; i++) arduino.digitalWrite(i, Arduino.HIGH);  //Setting Pins high

    minim = new Minim(this);  //we are working in this class
    song = minim.loadFile("Coldplay - Paradise.mp3");  //loading the file from the folder
    song.play();  //inbuilt library for playing audio mp3 files
    fft = new FFT(song.bufferSize(), song.sampleRate());    //FFT class created using fft library inbuilt in minim, provided be java
}
 
void draw() {      //this function is used for graphics and gives a blank window on the screen
    background(#151515);

    fft.forward(song.mix);

    strokeWeight(1.3);
    stroke(#FFF700);

    // frequency
    pushMatrix();
      translate(250, 0);   
      for(int i = 0; i < 0+fft.specSize(); i++) {
        line(i, height*4/5, i, height*4/5 - fft.getBand(i)*4); 
        if(i%100==0) text(fft.getBand(i), i, height*4/5+20);
        if(i==200) {
          if(fft.getBand(i)>2) {
            setColor1(255,255,0);
            setColor3(255,255,0);
          }
          else if(fft.getBand(i)>1) {
            setColor1(255,0,255);
            setColor3(255,0,255);
          } else {
            setColor1(255,255,255);
            setColor3(255,255,255);
          }
        }
        if(i==50) {
          if(fft.getBand(i)>5) {
            color_id = (color_id+1)%4;
          } else if(fft.getBand(i)>3) {
            if(color_id==0) setColor2(0,255,0);
            else if(color_id==1) setColor2(0,255,255);
            else if(color_id==2) setColor2(0,0,255);
            else setColor2(255,0,0);
          } 
          else {
            setColor2(255,255,255);
          }
        } 
      }  
    popMatrix();
    
    stroke(#FF0000);
  
    //waveform
    for(int i = 250; i < song.left.size() - 1; i++) {
      line(i, 50 + song.left.get(i)*50, i+1, 50 + song.left.get(i+1)*50);
      line(i, 150 + song.right.get(i)*50, i+1, 150 + song.right.get(i+1)*50);
      line(i, 250 + song.mix.get(i)*50, i+1, 250 + song.mix.get(i+1)*50);
    }
  
    noStroke();
    fill(#111111);
    rect(0, 0, 250, height);
  
    textSize(24);
    fill(#046700);
    text("left amplitude", 10, 50); 
    text("right amplitude", 10, 150); 
    text("mixed amplitude", 10, 250); 
    text("frequency", 10, height*4/5); 
}

void stop()
{
    for (int i = 0; i <= 13; i++) arduino.digitalWrite(i,arduino.HIGH);
    song.close();  
    minim.stop();
    super.stop();
}
void setColor1(int red, int green, int blue)
{
  if(common_cathode==1) {
    red = 255-red;
    green = 255-green;
    blue = 255-blue;
  }
  arduino.digitalWrite(redPin1, red);
  arduino.digitalWrite(greenPin1, green);
  arduino.digitalWrite(bluePin1, blue);  
}
void setColor2(int red, int green, int blue)
{
  if(common_cathode==1) {
    red = 255-red;
    green = 255-green;
    blue = 255-blue;
  }
  arduino.digitalWrite(redPin2, red);
  arduino.digitalWrite(greenPin2, green);
  arduino.digitalWrite(bluePin2, blue);  
}
void setColor3(int red, int green, int blue)
{
  if(common_cathode==1) {
    red = 255-red;
    green = 255-green;
    blue = 255-blue;
  }
  arduino.digitalWrite(redPin3, red);
  arduino.digitalWrite(greenPin3, green);
  arduino.digitalWrite(bluePin3, blue);  
}
